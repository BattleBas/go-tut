package ur

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
)

// Board defines the components needed for the game board
type Board struct {
	img *ebiten.Image
}

// NewBoard initializes the components needed for the game board
func NewBoard() (*Board, error) {
	img, _, err := ebitenutil.NewImageFromFile("assests/gameboard.png", ebiten.FilterDefault)
	if err != nil {
		return nil, err
	}

	b := &Board{}
	b.img = img

	return b, nil
}

// Draw draws the Ur board
func (b *Board) Draw(dst *ebiten.Image) {
	w, h := b.img.Size()
	mW, mH := dst.Size()
	scale := .45
	op := &ebiten.DrawImageOptions{}
	op.GeoM.Scale(scale, scale)
	op.GeoM.Translate((float64(mW)-float64(w)*scale)*.5, (float64(mH)-float64(h)*scale)*.5)
	dst.DrawImage(b.img, op)
}
