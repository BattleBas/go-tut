package ur

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
)

// RollButton will role the dice
type RollButton struct {
	img       *ebiten.Image
	mouseDown bool
	onPressed func()
	minX      int
	minY      int
	maxX      int
	maxY      int
	scale     float64
}

func NewRollButton(mW int, mH int) (*RollButton, error) {
	img, _, err := ebitenutil.NewImageFromFile("assests/button.png", ebiten.FilterDefault)
	if err != nil {
		return nil, err
	}

	b := &RollButton{}
	b.img = img

	b.scale = .45
	w, h := b.img.Size()
	b.minX = int((float64(mW) - float64(w)*b.scale) * .95)
	b.minY = int((float64(mH) - float64(h)*b.scale) * .05)
	b.maxX = b.minX + int(float64(w)*b.scale)
	b.maxY = b.maxY + int(float64(h)*b.scale)

	return b, nil
}

func (b *RollButton) Update() {
	if ebiten.IsMouseButtonPressed(ebiten.MouseButtonLeft) {
		x, y := ebiten.CursorPosition()
		if b.minX <= x && x < b.maxX && b.minY <= y && y < b.maxY {
			b.mouseDown = true
		} else {
			b.mouseDown = false
		}
	} else {
		if b.mouseDown {
			if b.onPressed != nil {
				b.onPressed()
			}
		}
		b.mouseDown = false
	}
}

func (b *RollButton) Draw(dst *ebiten.Image) {
	op := &ebiten.DrawImageOptions{}
	op.GeoM.Scale(b.scale, b.scale)
	op.GeoM.Translate(float64(b.minX), float64(b.minY))

	dst.DrawImage(b.img, op)
}
