package ur

import (
	"fmt"

	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
)

// Dice defines the components needed for the dice
type Dice struct {
	pip     *ebiten.Image
	noPip   *ebiten.Image
	results [4]bool
}

// NewDice initializes the components needed for the dice
func NewDice() (*Dice, error) {
	noPip, _, err := ebitenutil.NewImageFromFile("assests/dice_1.png", ebiten.FilterDefault)
	if err != nil {
		return nil, err
	}

	pip, _, err := ebitenutil.NewImageFromFile("assests/dice_2.png", ebiten.FilterDefault)
	if err != nil {
		return nil, err
	}

	dice := &Dice{}
	dice.pip = pip
	dice.noPip = noPip

	return dice, nil
}

func (d *Dice) Update() {
	fmt.Println("I rolled the dice!")
	d.noPip = d.pip

}

// Draw draws the dice
func (d *Dice) Draw(diceImage *ebiten.Image) {
	w, h := d.pip.Size()
	mW, mH := diceImage.Size()

	for i, r := range d.results {
		op := &ebiten.DrawImageOptions{}
		if r {
			op.GeoM.Translate((float64(mW)-float64(w))*.95-float64(i)*2, (float64(mH)-float64(h))*.93)
			diceImage.DrawImage(d.pip, op)
		} else {
			op.GeoM.Translate((float64(mW)-float64(w))*.95-80*float64(i), (float64(mH)-float64(h))*.93)
			diceImage.DrawImage(d.noPip, op)
		}
	}

}
