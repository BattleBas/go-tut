package ur

import (
	"image/color"

	"github.com/hajimehoshi/ebiten"
)

// Game stores the components needed for a game
type Game struct {
	board           *Board
	boardImage      *ebiten.Image
	dice            *Dice
	diceImage       *ebiten.Image
	rollButton      *RollButton
	rollButtonImage *ebiten.Image
	ScreenWidth     int
	ScreenHeight    int
}

// NewGame initializes the components needed for a game
func NewGame() (*Game, error) {
	g := &Game{
		ScreenWidth:  600,
		ScreenHeight: 420,
	}
	var err error
	g.board, err = NewBoard()
	if err != nil {
		return nil, err
	}
	g.dice, err = NewDice()
	if err != nil {
		return nil, err
	}
	g.rollButton, err = NewRollButton(600, 420)
	if err != nil {
		return nil, err
	}
	g.rollButton.onPressed = g.dice.Update

	return g, nil
}

// Update refreshes the screen
func (g *Game) Update(screen *ebiten.Image) error {
	g.rollButton.Update()
	return nil
}

// Draw draws the current game to the given screen
func (g *Game) Draw(screen *ebiten.Image) {
	if g.boardImage == nil {
		g.boardImage, _ = ebiten.NewImage(g.ScreenWidth, g.ScreenHeight, ebiten.FilterDefault)
		g.diceImage, _ = ebiten.NewImage(g.ScreenWidth, g.ScreenHeight, ebiten.FilterDefault)
		g.rollButtonImage, _ = ebiten.NewImage(g.ScreenWidth, g.ScreenHeight, ebiten.FilterDefault)
	}
	backgroundColor := color.RGBA{0xfa, 0xf8, 0xef, 0xff}
	screen.Fill(backgroundColor)

	g.board.Draw(g.boardImage)
	screen.DrawImage(g.boardImage, nil)

	g.dice.Draw(g.diceImage)
	screen.DrawImage(g.diceImage, nil)

	g.rollButton.Draw(g.rollButtonImage)
	screen.DrawImage(g.rollButtonImage, nil)

}

// Layout implements ebiten.Game's layout
func (g *Game) Layout(outsideWidth, outsideHeight int) (screenWidth, screenHeight int) {
	return g.ScreenWidth, g.ScreenHeight
}
