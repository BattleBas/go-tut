package main

import (
	"go-tut/ur"
	"log"

	"github.com/hajimehoshi/ebiten"
)

func main() {

	game, err := ur.NewGame()
	if err != nil {
		log.Fatal(err)
	}

	ebiten.SetWindowSize(game.ScreenWidth, game.ScreenHeight)
	ebiten.SetWindowTitle("The game of Ur")
	if err := ebiten.RunGame(game); err != nil {
		log.Fatal(err)
	}
}
